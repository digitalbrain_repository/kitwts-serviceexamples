#KITWTS Service 
Per utilizzare il pi� possibile quanto sviluppato e per coerenza con il resto dell'app consiglierei di utilizzare la stessa struttura degli oggetti utilizzati nel resto dell'app.

Per questo consiglio di utilizzare come response oggetti simili ai seguenti
##1. Story List Response
###Struttura della response

>  ***items*** : contiente la lista delle storie
>> - informazioni generali ex. (**id**, **description**, **enabled**, **publishDate**) 
>> - **viewed**: true/false se l'utente corrente ha visualizzato la storia
- **premium**: segnala se la storia coniente contenuti premium (sul layout viene visualizzata l'iconcina con la corona)
-  **publisher**: l'oggetto contiene tutte le informazione del publisher
-  **content**: array contenete i media della storia (video o immagini)

>>> - **type** : image/video
>>> - **aspect** : ratio del contenuto
>>> - **masks** : eventuale maskera da applicare al contenuto premium
>>> - **price** : oggetto prezzo (come per i post)
>>> - **views** : numero di visualizzazioni
>>> - **viewed** : true/false se l'utente corrente ha visualizzato il contenuto
>>> - **purchased** : true/false se l'utente ha acquistato il contenuto
>>> - **buys** : numero di utenti che hanno acquistato il contenuto
>>> - **viewers** : array contenente la lista degli utenti che hanno visualizzato
>>> - **buyers** : array contenente la lista degli utenti che hanno acquistato
>>> - **extras** : TBD 
>>> - **resource** : come per i post
>>> - **thumbnail** : come per i post

***Link File di esempio***
**[story_list_example.json](story_list_example.json)**
  
***Code***



```javascript
{
    "data": {
        "items": [

            {
                "id": "d38527e0-80cb-48eb-8f93-5df111f2e765",
                "description": "",
                "enabled": true,
                "publishDate": 1544800556953,
                "viewed": false,
                "premium": true,
                "publisher": {
                    "background": null,
                    "streamState": "none",
                    "description": "",
                    "followersCount": 20,
                    "followingsCount": 23,
                    "postsCount": 8,
                    "follow": "ACCEPTED",
                    "facebookUrl": null,
                    "private": false,
                    "active": true,
                    "email": "cesaremaltese@gmail.com",
                    "phone": null,
                    "gender": "NotSpecified",
                    "websiteLink": null,
                    "postLayout": "Not",
                    "id": "3fab241e-b0c9-46ca-8c14-f94b168bcc34",
                    "nickName": "cesare75",
                    "name": "cesare maltese",
                    "role": "Star",
                    "avatar": {
                        "id": "e78bfc1a-0141-41e9-a5c5-564461f0dbd9",
                        "contentType": "image/jpeg",
                        "formatUrls": {
                            "360": "https://...",
                            "480": "https://...",
                            "720": "https://...",
                            "1080": "https://...",
                            "original": "https://..."
                        },
                        "state": "COMPLETE"
                    }
                },
                "content": [{
                    "type": "video",
                    "aspect": 1,
                    "masks": [{
                        "maskSize": 20,
                        "isMuted": false,
                        "mask": [
                            0,
                            0,
                            0,
                            0,
                            0
                        ],
                        "offset": 4735
                    }],
                    "price": {
                        "applePriceId": "premium.level_099",
                        "androidPriceId": "premium.level_99",
                        "amount": "0.9900"
                    },
                    "views": 37,
                    "viewed": true,
                    "purchased": false,
                    "buys": 123,
                    "viewers": [{
                        "background": null,
                        "streamState": "none",
                        "description": "",
                        "followersCount": 20,
                        "followingsCount": 23,
                        "postsCount": 8,
                        "follow": "ACCEPTED",
                        "facebookUrl": null,
                        "private": false,
                        "active": true,
                        "email": "cesaremaltese@gmail.com",
                        "phone": null,
                        "gender": "NotSpecified",
                        "websiteLink": null,
                        "postLayout": "Not",
                        "id": "3fab241e-b0c9-46ca-8c14-f94b168bcc34",
                        "nickName": "cesare75",
                        "name": "cesare maltese",
                        "role": "Star",
                        "avatar": {
                            "id": "e78bfc1a-0141-41e9-a5c5-564461f0dbd9",
                            "contentType": "image/jpeg",
                            "formatUrls": {
                                "360": "https://...",
                                "480": "https://...",
                                "720": "https://...",
                                "1080": "https://...",
                                "original": "https://..."
                            },
                            "state": "COMPLETE"
                        }
                    }],
                    "buyers": [{
                        "background": null,
                        "streamState": "none",
                        "description": "",
                        "followersCount": 20,
                        "followingsCount": 23,
                        "postsCount": 8,
                        "follow": "ACCEPTED",
                        "facebookUrl": null,
                        "private": false,
                        "active": true,
                        "email": "cesaremaltese@gmail.com",
                        "phone": null,
                        "gender": "NotSpecified",
                        "websiteLink": null,
                        "postLayout": "Not",
                        "id": "3fab241e-b0c9-46ca-8c14-f94b168bcc34",
                        "nickName": "cesare75",
                        "name": "cesare maltese",
                        "role": "Star",
                        "avatar": {
                            "id": "e78bfc1a-0141-41e9-a5c5-564461f0dbd9",
                            "contentType": "image/jpeg",
                            "formatUrls": {
                                "360": "https://...",
                                "480": "https://...",
                                "720": "https://...",
                                "1080": "https://...",
                                "original": "https://..."
                            },
                            "state": "COMPLETE"
                        }
                    }],
                    "extras": [{
                        "tbd"
                    }],
                    "resource": {
                        "id": "8a083589-af8f-4286-817d-ccf3482c2e50",
                        "contentType": "video/mp4",
                        "formatUrls": {
                            "original": "https://...",
                            "playlist": "https://..."
                        },
                        "state": "COMPLETE"
                    },
                    "thumbnail": {
                        "id": "320f913f-f790-4c80-8a89-76ea716167a8",
                        "contentType": "image/jpeg",
                        "formatUrls": {
                            "360": "https://...",
                            "480": "https:/...",
                            "720": "https://...",
                            "1080": "https://...",
                            "original": "https://..."
                        },
                        "state": "COMPLETE"
                    }

                }]

            }

        ]
    },
    "code": "success"
}
```